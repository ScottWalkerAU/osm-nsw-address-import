#!/usr/bin/env node
var fs = require('fs')
var osm_geojson = require('./node_modules/osm-and-geojson/osm_geojson.js')

input_dir = '../cleaned/'
output_dir = '../cleaned/'

fs.readdir(input_dir, (error, files) => {
    files.forEach(filename => {
        if (filename.includes('geojson')) {
            fs.readFile(input_dir + filename, 'utf8', function(error, data) {
                fs.writeFileSync(output_dir + filename.replace('.geojson', '.osm'), osm_geojson.geojson2osm(data))
            });
        }
    });
})
